# Minecraft Region Plot

Simple tool to plot a list of Minecraft region filenames as squares, to determine which files to prune.

![Screenshot](screenshot.png){width=640}

# To run

```
yarn install
yarn start-dev
```

http://localhost:3000

# To use

Paste a list of region filenames into the first textarea, e.g.
```
r.0.0.mca   r.0.-2.mca   r.1.0.mca     r.-1.-1.mca  r.-12.-4.mca  r.1.-2.mca    r.-2.0.mca   r.-3.0.mca   r.-7.-3.mca
r.0.-1.mca  r.10.-4.mca  r.-11.-4.mca  r.-1.1.mca   r.-12.-5.mca  r.-13.-4.mca  r.-2.-1.mca  r.-3.-1.mca  r.-7.-4.mca
r.0.1.mca   r.-1.0.mca   r.-11.-5.mca  r.1.-1.mca   r.-1.-2.mca   r.1.-3.mca    r.-2.-2.mca  r.-3.-2.mca
```

Go around the dimension whose filenames you've captured (Nether is DIM-1, End is DIM1), and record any points of interest you want to reference by their (x,z) coordinates.

Paste these into the second textarea, along with a description for each. Use a newline to separate these. e.g.
```
0 0 origin
-750, -250 tower
-451 -181 blaze
-412 -131 fort
-633 -246 rail home
-562 -217
-492 -198
705 -250 ice plain
```

Then click the Plot button.

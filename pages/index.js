import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React, { useState } from 'react';
import styled from '@emotion/styled';

const SCALE = 1/4;

const Page = styled.div`
  display: flex;
  flex-flow: row nowrap;
  width: 100%;
  height: 100vh;
`;

const FieldContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  textarea {
    flex-grow: 1;
  }
  padding-bottom: 20px; /* dumbass scrollbar */
`;

const GridContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;


const RegionBox = styled.div`
  box-sizing: border-box;
  border: 3px solid red;
  background: #ffbfbf;
  color: #888;
  width: ${512 * SCALE}px;
  height: ${512 * SCALE}px;

  position: absolute;
  left: ${props => props.x * SCALE}px;
  top: ${props => props.y * SCALE}px;
`;

// Each Region is 512 blocks square. It holds 32x32 chunks, 16 blocks across each.
const Region = ({ name, x, y }) => {
  return (
    <RegionBox x={x} y={y} >
      <span>{name}</span>
    </RegionBox>
  );
};


const PoiLabel = styled.div`
  box-sizing: border-box;
  color: black;

  position: absolute;
  left: ${props => props.x * SCALE + 10}px;
  top: ${props => props.y * SCALE - 5}px;
`;
const PoiDot = styled.span`
  width: 2px;
  height: 2px;
  border: 2px solid black;
  border-radius: 2px;

  position: absolute;
  left: -10px;
  top: +5px;
`;


export default function Home() {
  const [regions, setRegions] = useState([]);
  const [pois, setPois] = useState([]);

  const plot = () => {
    console.log("Plotting...");
    const filenames = document.getElementById('regionfiles').value.split(/\s+/);
    setRegions(filenames.map(filename => {
      const matched = filename.match(/.*\/?(r\.([0-9-]+)\.([0-9-]+)\.mca$)/);
      if ( ! matched) { return; }
      const rx = Number.parseInt(matched[2]);
      const ry = Number.parseInt(matched[3]);
      
      return {
        name: matched[1],
        rx,
        ry,
        x: rx * 512,
        y: ry * 512,
      };
    }).filter(region => region));

    const poiLines = document.getElementById('poi').value.split("\n");
    setPois(poiLines.map(text => {
      const matched = text.match(/\s*([0-9-]+)\s*,?\s*([0-9-]+)\s*(.*)\s*/);
      if ( ! matched) { return; }
      const x = Number.parseInt(matched[1]);
      const y = Number.parseInt(matched[2]);
      
      return {
        name: matched[3],
        x,
        y,
      };
    }).filter(poi => poi));
  };

  let leftx = 0;
  let topy  = 0;
  regions.forEach(region => {
    leftx = Math.min(region.x, leftx);
    topy  = Math.min(region.y, topy);
  });
  const regionComponents = regions.map(region => (
    <Region {...region} key={region.name} x={region.x - leftx} y={region.y - topy} />
  ));
  const poiComponents = pois.map(poi => (
    <PoiLabel x={poi.x - leftx} y={poi.y - topy} key={`label:${poi.x}:${poi.y}`}>
      {poi.name}
      <PoiDot x={poi.x - leftx} y={poi.y - topy} key={`dot:${poi.x}:${poi.y}`} />
    </PoiLabel>
  ));

  return (
    <div className={styles.container}>
      <Head>
        <title>MC Region Plot</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Page>
        <FieldContainer>
          <label htmlFor="regionfiles">Region Filenames</label>
          <textarea id="regionfiles" cols="30" />
          <label htmlFor="poi">Points Of Interest (x,z)</label>
          <textarea id="poi" cols="30" />
          <button onClick={plot}>Plot</button>
        </FieldContainer>

        <GridContainer>
          {regionComponents}
          {poiComponents}
        </GridContainer>
      </Page>

    </div>
  );
};
